# Astronomy Picture Of the Day for Ubuntu Gnome Shell

Get, each day, Astronomy Picture of the Day (https://apod.nasa.gov/apod) as desktop background image :
- Will create, if not exist, *${HOME}/Images/apod* path
- Get Astronomy Picture Of the Day if not already on disk as *${HOME}/Images/apod/$(date +%Y%m%d)_${image_name}* and if really a picture (can be a video, from time to time)
- Get Picture description as *${HOME}/Images/apod/$(date +%Y%m%d)_${image_name%.&ast;}_desc.html*
- Update Gnome Desktop Wallpaper with the new Picture
- Send a notification to Gnome Shell
- Update Lock Screen background with a randomly picked picture in *${HOME}/Images/apod/*


To make it work :
- Clone Git repo
- Make the apod.bash file executable 
- Then :
  - Add it as a [startup application for Gnome](https://stackoverflow.com/questions/8247706/start-script-when-gnome-starts-up)
  - Manually execute the script
  - Add it to ~/.profile file (not tested)


Tested on :
- Ubuntu 18.10
- Ubuntu 19.04
- Ubuntu 19.10

