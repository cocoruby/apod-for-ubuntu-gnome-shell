#!/bin/bash

apod_images_path="${HOME}/Images/apod"
if [ ! -d "$apod_images_path" ]; then mkdir -p $apod_images_path; fi

# Get DBUS address
compatiblePrograms=("nautilus" "kdeinit" "kded4" "pulseaudio" "trackerd")
# Attempt to get a program pid
for index in ${compatiblePrograms[@]}; do
    PID=$(pidof -s ${index})
    if [[ "${PID}" != "" ]]; then
        break
    fi
done
if [[ "${PID}" == "" ]]; then
    echo "Could not detect active login session"
    exit 1
fi
QUERY_ENVIRON="$(tr '\0' '\n' < /proc/${PID}/environ | grep "DBUS_SESSION_BUS_ADDRESS" | cut -d "=" -f 2-)"
if [[ "${QUERY_ENVIRON}" != "" ]]; then
    export DBUS_SESSION_BUS_ADDRESS="${QUERY_ENVIRON}"
    echo "Connected to session:"
    echo "DBUS_SESSION_BUS_ADDRESS=${DBUS_SESSION_BUS_ADDRESS}"
else
    echo "Could not find dbus session ID in user environment."
    exit 1
fi

ls ${apod_images_path}/$(date +%Y%m%d)* 1>/dev/null 2>&1
if [ $? -ne 0 ]
then
  # Get APOD Page
  page_content=`wget http://apod.nasa.gov/apod/ -O -`
  
  # Get APOD Image
  image_uri=`echo "$page_content" | grep href=\"image | sed s/.*=\"//g | sed s/\".*//g`
  image_name=$(echo $image_uri | sed -E 's#.*?/(.*)#\1#g')
  if [ "$image_name" != "" ]; then
    wget http://apod.nasa.gov/apod/$image_uri -O "$apod_images_path/$(date +%Y%m%d)_$image_name"

    # Get APOD Text
    image_description=`echo "$page_content" | grep -A 15 href=\"image | grep -B 5 "Image Credit" | grep -v "Image Credit" | tr '\n' ' ' | sed -E 's#.*<b>(.*?)</b>.*#\1#g' | sed -e 's/^[[:space:]]*//' | sed -e 's/[[:space:]]*$//'`
    image_explanation=`echo "$page_content" | grep -A50 "Explanation:" | grep -B50 "<center>" | grep -v "<center>"`
    echo "<h1>$image_description</h1>" > $apod_images_path/$(date +%Y%m%d)_${image_name%.*}_desc.html
    echo -e "$image_explanation" >> $apod_images_path/$(date +%Y%m%d)_${image_name%.*}_desc.html


    # Update Wallpaper
    gsettings set org.gnome.desktop.background picture-uri file:///$apod_images_path/$(date +%Y%m%d)_$image_name

    # Send notificaiton
    [ $? -eq 0 ] && notify-send ' ' "<b>APOD : <u>$image_description</u></b> ($apod_images_path/$(date +%Y%m%d)_${image_name%.*}_desc.html - http://apod.nasa.gov/apod/)"
  fi
fi

# Update Lock Screen with random image
nb_bgs=$(ls $apod_images_path | grep -v html | wc -l)
bg_number=$(( ( RANDOM % $nb_bgs )  + 1 ))
lock_image_name=$(ls $apod_images_path | grep -v html | head -n $bg_number | tail -n 1)
gsettings set org.gnome.desktop.screensaver picture-uri file:///$apod_images_path/$lock_image_name

